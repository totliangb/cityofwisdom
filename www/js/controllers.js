angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Dash) {

  $scope.banners = Dash.banners();
  Dash.getPullRequests().then(function(result){
      $scope.data = result;
  },function(error){
      $scope.data = error;
  },function(progress){
      $scope.progress = progress;
      $scope.show = false;
  });
})

.controller('PostCtrl', function($scope) {

  $scope.openCamera = function() {
    var srcType = Camera.PictureSourceType.CAMERA;
    var options = setOptions(srcType);

    navigator.camera.getPicture(function cameraSuccess(imageUri) {

      displayImage(imageUri);

    }, function cameraError(error) {
      console.debug("Unable to obtain picture: " + error, "app");

    }, options);
  }

  function setOptions(srcType) {
    var options = {
      // Some common settings are 20, 50, and 100
      quality: 50,
      destinationType: Camera.DestinationType.FILE_URI,
      // In this app, dynamically set the picture source, Camera or photo gallery
      sourceType: srcType,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true  //Corrects Android orientation quirks
    }
    return options;
  }

  function displayImage(imgUri) {

    var elem = document.getElementById('imageFile');

    insertText = elem.innerHTML() + "<img src='"+imgUri+"' />";

    elem.innerHTML(insertText);
  }

})

.controller('ChatsCtrl', function($scope, $state, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
  $scope.goChatDetail = function(chat){
    $state.go("tab.chat-detail",{chatId:chat},{reload:true});
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('NewsCtrl', function($scope, $state, News) {
  $scope.news = News.all();
  $scope.remove = function(news_item) {
    News.remove(new_item);
  };
  $scope.goNewsDetail = function(news_item) {
    $state.go("tab.news-detail",{newId:news_item},{reload:true});
  };
})

.controller('NewDetailCtrl', function($scope, $stateParams, News) {
  $scope.new_item = News.get($stateParams.newId);
})

.controller('GroupsCtrl', function($scope, $timeout, $state, $http, $ionicLoading, $ionicScrollDelegate, Groups) {
  $scope.groups = Groups.all();
  $scope.remove = function(group_item) {
    Groups.remove(group_item);
  };

  /**
   * 字母索引
   * 来源：http://blog.csdn.net/lishihong108/article/details/53422950?locationNum=5&fps=1
   */
  
    // var chars="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var chars = Groups.getIniData();

    $scope.searchEmptyShow=false;//是否显示清除输入的图标
    $scope.showMiddle=false; //是否在屏幕中央显示选中的字母索引
    $scope.hIndex=(document.documentElement.clientHeight-100-44-35-4)/chars.length;//右边侧边栏每个字母的高度，是屏幕高度减去标题栏的44，减去页面样式中的margin-top:2px，margin-bottom:2px,再除以26，这样以保证在各个手机屏幕上的字母的距离的均等性
    $scope.indexs=[];
    // loadData();
    $scope.cityDatas = Groups.all();
    for(var i=0;i<chars.length;i++){
        $scope.indexs.push(chars[i]);//获取字母数组 
    }
    $scope.startDot=function(){//判断清除输入框的图标是否显示
        $scope.cityName = document.getElementById('group_search').value;
        if($scope.cityName.length=0 || $scope.cityName==""){
            $scope.searchEmptyShow=false;
        }else{
            $scope.searchEmptyShow=true;
        }
    };
    $scope.searchEmpty=function(){//点击清除输入框的图标的点击事件
        $scope.searchEmptyShow=false;
        $scope.cityName="";
        document.getElementById('group_search').value='';
    };

    $scope.mTouch=function(event){ 
      var positionX=event.pageX || event.touches[0].pageX;
      var positioinY=event.pageY || event.touches[0].pageY;
      var ele = document.elementFromPoint(positionX,positioinY);  
      if(!ele){
        return;
      }
      var c=ele.innerText;
      if(!c || c==" " || c.length!=1){
        return;
      }
      $scope.hint=c; 
      $scope.showMiddle=true;

      var scroll = document.getElementById("city_"+$scope.hint).offsetTop -    $ionicScrollDelegate.getScrollPosition().top; 
      $ionicScrollDelegate.scrollBy(0,scroll,true);
      var ele = document.getElementsByTagName("ion-content");  
      ele[0].style.overflow="auto";  //解决滑动右边的导航字母后，左边不能再滚动的bug，可以试着注释这两句来测试这个问
      
      $scope.mRelease();
    };

    $scope.mRelease=function(){
        $timeout(function(){
            $scope.showMiddle=false;
        },300); 
    };
    // function loadData(){//从本地的一个包含全国各城市的json文件中加载数据
    //     // $ionicLoading.show(); 
    //     $http.get("http://www.cityofwisdom.dev/json/city.json").success(function(data) {
    //      $scope.cityDatas=data.dataList;
    //      // $ionicLoading.hide();
    //  });
    // } 
})
.controller('GroupDetailCtrl', function($scope, $stateParams, Groups) {
  $scope.group = Groups.get($stateParams.groupId);
})

.controller('WorksCtrl', function($scope, $state, Works) {
  $scope.works = Works.all();
  $scope.remove = function(work_item) {
    Works.remove(work_item);
  };
  $scope.goWorkDetail = function(work_item){
    $state.go("tab.work-detail",{workId:work_item},{reload:true});
  }
})
.controller('WorkDetailCtrl', function($scope, $stateParams, Works) {
  $scope.work = Works.get($stateParams.workId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
