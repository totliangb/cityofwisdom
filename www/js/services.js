angular.module('starter.services', [])

.factory('Dash', function($http, $q) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var banners = [{
    id: 0,
    title: 'Ben Sparrow',
    url: 'You on your way?',
    imgSrc: 'img/ben.png'
  }, {
    id: 1,
    title: 'Max Lynx',
    url: 'Hey, it\'s me',
    imgSrc: 'img/max.png'
  }, {
    id: 2,
    title: 'Adam Bradleyson',
    url: 'I should buy a boat',
    imgSrc: 'img/adam.jpg'
  }, {
    id: 3,
    title: 'Perry Governor',
    url: 'Look at my mukluks!',
    imgSrc: 'img/perry.png'
  }];
  var userService = {};   // 返回的服务

  // this.urlForRandomUser = "https://randomuser.me/api/";

  // var that = this; // 获取当前作用域，下面要用到
  // userService.getRandomUser = function () {

  //     var defered = $q.defer();

  //     $http.get(that.urlForRandomUser)
  //         .success(function (data) {
  //             defered.resolve(data.results[0]);
  //         })
  //         .error(function (err) {
  //             defered.reject(err);
  //         });
  //     return defered.promise; // 把defered对象中的promise对象返回出来
  // };
  userService.getPullRequests = function(){
        var deferred = $q.defer();
        var promise = deferred.promise;
        var progress;
        $http.get("http://www.cityofwisdom.dev/API/index/test")
        .success(function(data){
            // var result = [];
            // for(var i = 0; i < data.length; i++){
            //     result.push(data[i].user);
            //     progress = (i+1)/data.length * 100;
            //     deferred.notify(progress);
            // }
            deferred.resolve(data);
            })
        .error(function(error){
            deferred.reject('error!');
        });
        return promise;
    }
  userService.banners =  function(){
      return banners;
  }

  return userService;
})

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Groups', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var groups = [{
    id: 0,
    face: 'img/mike.png',
    iniData : 'A',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png',
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png',
    }]
  }, {
    id: 1,
    face: 'img/mike.png',
    iniData : 'B',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png',
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png',
    }]
  }, {
    id: 2,
    face: 'img/mike.png',
    iniData : 'C',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png',
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png',
    }]
  }, {
    id: 3,
    face: 'img/mike.png',
    iniData : 'D',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png',
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png',
    }]
  }, {
    id: 4,
    iniData : 'E',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png',
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png',
    }]
  }, {
    id: 5,
    face: 'img/mike.png',
    iniData : 'F',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png'
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png'
    }]
  }, {
    id: 6,
    face: 'img/mike.png',
    iniData : 'G',
    datas : [{
      "zoneCity": "安阳市",
      "zoneCityId": "372",
      'face': 'img/mike.png',
    },{
      "zoneCity": "鞍山市",
      "zoneCityId": "412",
      'face': 'img/mike.png',
    }]
  }];

  return {
    all: function() {
      return groups;
    },
    getIniData: function() {
      var iniDataObj = [];
      for (var i = 0; i < groups.length; i++) {
        iniDataObj.push(groups[i].iniData);
      }
      return iniDataObj.sort();
    },
    remove: function(group) {
      groups.splice(groups.indexOf(group), 1);
    },
    get: function(groupId) {
      for (var i = 0; i < groups.length; i++) {
        if (groups[i].id === parseInt(groupId)) {
          return groups[i];
        }
      }
      return null;
    }
  };
})

.factory('Works', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var works = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return works;
    },
    remove: function(work) {
      works.splice(works.indexOf(work), 1);
    },
    get: function(workId) {
      for (var i = 0; i < works.length; i++) {
        if (works[i].id === parseInt(workId)) {
          return works[i];
        }
      }
      return null;
    }
  };
})
.factory('News', function() {
  
})

.factory('News', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var news = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return news;
    },
    remove: function(new_item) {
      news.splice(news.indexOf(new_item), 1);
    },
    get: function(newId) {
      for (var i = 0; i < news.length; i++) {
        if (news[i].id === parseInt(newId)) {
          return news[i];
        }
      }
      return null;
    }
  };
});

